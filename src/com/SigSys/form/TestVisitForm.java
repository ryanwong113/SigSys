package com.SigSys.form;

public class TestVisitForm {

	private String numberOfTestVisits;
	
	public TestVisitForm() {
		
	}
	
	public String getNumberOfTestVisits() {
		return numberOfTestVisits;
	}
	
	public void setNumberOfTestVisits(String numberOfTestVisits) {
		this.numberOfTestVisits = numberOfTestVisits;
	}
	
}
